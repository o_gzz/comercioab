import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  tarjetas  = [
    {
      titulo: 'Limpiador de hornos',
      subtitulo: 'Limpiador',
      descripcion: 'Marca Easy Off 500ml',
      img: 'assets/img/card4.jpg'
    },
    {
      titulo: 'Desinfectante en aerosol',
      subtitulo: 'Desinfectante',
      descripcion: 'Marca Cif 200ml.',
      img: 'assets/img/card5.jpg'
    },
    {
      titulo: 'Limpiador líquido brasso',
      subtitulo: 'Limpiador',
      descripcion: 'Marca Antigrasa  700ml.',
      img: 'assets/img/card3.jpg'
    },
    {
      titulo: 'Limpiador líquido',
      subtitulo: 'Limpiador multiusos ',
      descripcion: 'Marca Fabuloso Antibaterial 5L',
      img: 'assets/img/card1.jpg'
    },
    {
      titulo: 'Limpiador líquido amonia',
      subtitulo: 'Limpiador',
      descripcion: 'Marca Ajax 2L',
      img: 'assets/img/card2.jpg'
    }
    
  ];
}
