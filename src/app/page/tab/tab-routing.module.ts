import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabPage } from './tab.page';

const routes: Routes = [
  {
    path: '',
    component: TabPage,
    children: [
      {
        path: 'inicio',
        loadChildren: () => import('./../../page/inicio/inicio.module').then( m => m.InicioPageModule)
      },
      {
        path: 'categorias',
        loadChildren: () => import('./../../page/categorias/categorias.module').then( m => m.CategoriasPageModule)
      },
      {
        path: 'carrito',
        loadChildren: () => import('./../../page/carrito/carrito.module').then( m => m.CarritoPageModule)
      },
      {
        path: 'cuenta',
        loadChildren: () => import('./../../page/cuenta/cuenta.module').then( m => m.CuentaPageModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabPageRoutingModule {}
